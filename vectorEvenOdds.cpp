#include <iostream>
#include <vector>

int main() {
  std::vector<int> nums = {2, 4, 3, 6, 1, 9};
  int even = 0;
  int odd = 1;
  for(int i = 0; i < nums.size(); i++){
    if (nums[i] % 2 == 0) even += nums[i];
    else odd = odd * nums[i];
  }

  std::cout << "Sum of even numbers is " << even << std::endl;
  std::cout << "Product of odd numbers is " << odd << std::endl;
}