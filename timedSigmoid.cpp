#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>

double sigmoid(double t, double k, double t0) {
    return 1.0 / (1.0 + exp(-k * (t - t0)));
}

void printSigmoidRecursive(double t, double k, double t0, const std::chrono::steady_clock::time_point& startTime) {
    double sigmoidValue = sigmoid(t, k, t0);
    std::cout << "sigmoid(" << t << ") = " << sigmoidValue << std::endl;
    t += 0.001;
    
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    std::chrono::steady_clock::time_point currentTime = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsedSeconds = currentTime - startTime;
    if (elapsedSeconds.count() >= 10.0) return;
    
    printSigmoidRecursive(t, k, t0, startTime);
}

int main() {
    double t = 0.0; double k = 1.0; double t0 = 0.0;
    std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();
    printSigmoidRecursive(t, k, t0, startTime);
}